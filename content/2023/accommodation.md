---
title: Accommodation
menu:
  "2023":
    parent: travel
    weight: 2
hideSponsors: true
layout: single-in-container
---
Thessaloniki is a city with a wide variety of hotels, hostels, and other types of accommodations to suit all budgets and preferences. In collaboration with the University of Macedonia (UoM), we have compiled a list of recommended hotels that offer exclusive prices for Akademy’s attendees. However, if these options don't fit your needs or preferences, you can also explore various other platforms to find the perfect accommodation for your stay in Thessaloniki.

To ensure a comfortable and stress-free stay during Akademy in the summer, we highly recommend booking your accommodation in advance. This will not only save you from the hassle of last-minute planning but will also guarantee that you have a place to stay during the event.

Please mention at the hotel in advance that you are booking  a room for Akademy event from KDE Community that is organized at University of Macedonia (UoM) in order to have a special offer.

**STAY HYBRID HOSTEL**

* Website: *[Stay Hybrid Hostel Website](https://www.thestay.gr/)*
* Distance: 2.3 km from UoM
* Room Prices (These are the individual room prices - -10% is applied):

  * 36 € Double room for private use (with WC)
  * 43 € double room double (with WC)
  * 48 € triple room (with WC)
  * 70 € 4 bed deluxe
* **Special offer for KDE:**

  * \-10% on normal rates (require a min. of 20 people group booking confirmation no later than April 15th 2023)

**COLORS URBAN HOTEL**

* Website: *[Colors Urban Hotel Website](https://colorshotel.gr/en/)*
* Distance: 2.3 km from UoM
* **Special offer for KDE:**

  * 10% off all room rates

> **C﻿ODE:** KDE2023

**ONOMA HOTEL**

* Website: *[Onoma Hotel Website](https://onomahotel.com)*
* Distance: 3.1 km from UoM
* **Special offer for KDE:**

  * 103 € smart name room (18sqm)
  * 123 € first name (21sqm) (double)

> **C﻿ODE:** KDE2023

**PARK HOTEL**

* Website: *[Park Hotel Website](https://parkhotel.com.gr/)*
* Distance: 2.3 km from UoM
* **Special offer for KDE:**

  * 65 € single room
  * 75 € double room
  * 89 € triple room

> **C﻿ODE:** Akademy 2023

**CAPSIS HOTEL**

* Website: *[Capsis Hotel Website](https://capsishotels.gr/en/)*
* Distance: 3.1 km from UoM
* **Special offer for KDE:**

  * 70 € economy
  * 75 € business
  * 90 € executive

> **CODE:** KDE 2023
>
> [Request via Email](mailto:reservations@capsishotel.gr)

**ANATOLIA HOTEL**

* Website: *[Anatolia Hotel Website](https://anatoliabaggage.com/)*
* Distance: 3 km from UoM
* **Special offer for KDE:**

  * 120 € junior suite
  * 140 € grand suite

> **C﻿ODE:** KDE2023

**ELECTRA PALACE**

* Website: *[Electra Palace Website](http://electra-palace.thessaloniki.top-hotels-gr.com)*
* Distance: 2.1 km from UoM
* **Special offer for KDE:** 

  10% off all room rates

> **C﻿ODE:** Akademy 2023

**MAKEDONIA PALACE**

* Website: *[Macedonia Palace Website](https://makedoniapalace.com)*
* Distance: 1.4 km from UoM
* **Special offer for KDE:**

  * 155 € (single room / city view)
  * 175 € (double room / city view)
  * 185 € (single room / sea view)
  * 205 € (double room / sea view)

> **CODE:** AKADEMY2023
>
> **[Booking link](https://makedoniapalace.reserve-online.net/?lang=en_US&checkin=2023-07-14&_=&nights=7&adults=1&voucher=AKADEMY2023&rooms=1 "https\://makedoniapalace.reserve-online.net/?lang=en_US&checkin=2023-07-14&\_=&nights=7&adults=1&voucher=AKADEMY2023&rooms=1")** (link will be open until 15/05/2023)

**The MET HOTEL**

* Website: *[The Met Hotel Website](https://www.themethotel.gr/)*
* Distance: 4 km from UoM
* **Special offer for KDE:**

  * 120 € (single room)
  * 130 € (double room)

  > **CODE:** KDE AKADEMY 2023
  >
  > [Reservation Email](mailto:Resv1.themet@chandris.gr)

**Queen Olga Hotel**

* Website: ***[Queen Olga Website](https://www.queenolga.gr "https\://www.queenolga.gr")***
* Distance: 1,9 km from UoM
* **Special offer for KDE:**

  * 55€ (single room inc. breakfast)
  * 75€ (double room inc. breakfast)

> **CODE:** KDE Akademy
>
> [Reservation Email](mailto:reservations@queenolga.gr "mailto\:reservations@queenolga.gr")

**ABC Hotel**

* Website: *[ABC Hotel Website](https://hotelabc.gr)*
* Distance: 950 m from UoM
* Special offer for KDE:

  * 60€ (single room inc. breakfast)
  * 77€ (double/twins room inc. breakfast)
  * 97€ (triple room inc. breakfast)

> **CODE:** AKADEMY 2023
>
> [Request By Email](mailto:info@hotelabc.gr)