---
title:       "Venue"
date:        2010-03-16
changed:     2016-11-02
menu:
  "2010":
    weight: 3
scssFiles:
- /scss/2010/images.scss
---

<h2 style="margin-top: 15px;">The University of Tampere</h2>

The **main conference session** (Saturday 3rd July and Sunday 4th July) and the **KDE e.V. General Assembly** (Monday 5th July) will be held in the main building of the University of Tampere (from the map: in A and D wings). The main campus is close to the Tampere city center, and about ten minutes walk from the railway station.

*Address:  
University of Tampere  
Kalevantie 4  
33014 University of Tampere*

![](/media/2010/map_uni_tampere.gif)

The University of Tampere embraces many fields of science and its research profile is extensive and multidisciplinary. There are six faculties and nine independent institutes. The University of Tampere is the biggest provider of higher education in Finland for social sciences and the accompanying administrative sciences. About 15,200 students are currently pursuing degrees there. Every year approximately one thousand master's degrees and one hundred doctoral degrees are produced.

<h2>Finlayson Area</h2>

<img class="framed" style="float: right; width: 50%;" src="/media/2010/pic_finlayson.preview.jpg" alt="" />

<p>Pre-registration and Welcoming party (Friday 2nd July), workshops and BoFs will be organized in the Finlayson area, mainly in the Demola &amp; Protomo premises. The Finlayson area is in the city center, just next to the Centre Square.</p>

*Demola &amp; Protomo:  
Väinö Linnan aukio 15, 3rd floor  
33210 Tampere*

<h3>About Demola</h3>
<p>Focusing on ICT intensive and digital media services with global market potential, Demola facilitates tens of multidisciplinary innovation teams on an annual basis. Demola is an opportunity for students to contribute real-life innovations with end-users and globally connected organizations.</p>
<h3>About Protomo</h3>
<p>Protomo is a multidisciplinary innovation community for entrepreneurs of the future. Protomo provides new product and service ideas with free facilities, community support and sparring form leading industry experts. In Protomo you can create prototypes of new products and services and develop your spin-off business idea without immediate business risks, together with your potential customers.</p>
<h3>Finlayson Area</h3>
<p>The historical Finlayson mill area is known as the old "town within the town" of Tampere. The Finlayson area, delighting in its historical roots, has become an excellent tourist attraction with versatile services for everyone to enjoy: there are museums, art galleries, theaters, boutiques, a cinema complex and numerous restaurants.</p>
