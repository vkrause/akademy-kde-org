---
title:       "Hands-on Qt for mobile"
date:        2010-06-24
changed:     2010-07-01
menu:
  "2010":
    parent: program
    weight: 5
scssFiles:
- /scss/tables.scss
---
<p>Meet Nokia Qt experts and hear about Qt tools and outlook on future development and how to develop applications on desktop and mobile.</p>
<p>This workshop will take place at <strong>Demola</strong>.</p>

<table>
<tbody>
<tr>
<th>1:00pm</th> <th>Opening</th>
<td>
<ul>
<li>Why open source is the hot topic of today?</li>
<li>Open Mobile Development Ecosystem - with Qt</li>
<li>Nokia Qt SDK - cross platform toolset for mobile development</li>
</ul>
</td>
</tr>
<tr>
<th>2:00pm</th> <th>Let's hack</th>
<td>
<ul>
<li>Develop your own mobile application with Qt</li>
<li>Support and tuition available by Nokia experts</li>
</ul>
</td>
</tr>
<tr>
<th>7:00pm</th> <th>Closing</th>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
