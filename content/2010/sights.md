---
title:       "Sights and Attractions"
date:        2010-03-30
changed:     2010-07-05
menu:
  "2010":
    parent: social
    name: Sightseeing
    weight: 3
---
<p>Founded in 1779 on the banks of the Tammerkoski Rapids - one of Finland's national heritage landscapes - Tampere evolved into the most highly industrialised locality in Finland during the 19th century.  Thanks to the new city planning, former industrial areas such as Finlayson and Tampella are alive again: cafés, restaurants, shops, sport and fitness centres, dramatic and cinema theatres, workshops and galleries have found locations there. Besides the rich industrial heritage, there is a lot to see in Tampere: dozens of museums, galleries and exhibitions, original Finnish architecture, breathtaking lake sceneries, magnificent glacial ridges, beautiful parks and lush forests next to the city centre.  More information: <a href="http://www.gotampere.fi/eng/sights" data-proofer-ignore>http://www.gotampere.fi/eng/sights</a></p>

If you would like to see more of Tampere, <a href="http://www.eattampere.com/">E.A.T. Tampere</a> also offers walking and bike tours with special prices for Akademy attendees. Please read below for details:

<ul>
<li><b>Walking tour - Tampere</b>: Daily. Free Tour at 12.00 starting from keskustori in front of the theatre.  How does a Tampere Free Tour work? Well the idea is simple, guides work on a tips only basis and earn according to performance so you decide what to give in the end of the tour.  More information about this tour can be found on <a href="http://www.tamperefreetour.com">http://www.tamperefreetour.com</a></li>
<li><b>Bike &amp; Sauna tour</b>: Monday 16.30 Tuesday 19.00 Wednesday 19.00 Friday 16.30.  A guide will take you on a cycling tour in Tampere and Kauppi Forest. We will also go to a real Finnish sauna next to the lake. This is a mixed public sauna where you have to wear your bathing clothes. You need to bring you bathing clothes, towel and something to drink from such as a water bottle. The tour lasts about 2 ½ hours including a 1 hour visit to the sauna. During sauna you have the change to take a swim in the lake to cool down.  Normal price for this tour is EUR 25,-. Participants of the congress can join for EUR 22,50 and if 6-12 persons from the congress are joining it will cost EUR 20,- a person.  This includes the rent of the bike and helmet, entrance to the sauna and guiding.</li>
<li><b>Beautiful bike tour</b>: Sunday 19.30  We start in the city center and pass some remarkable building before we leave the centre. The tour continues to the steep Pispala area where we can find beautiful wooden houses and great views on the lake. The guide will show hidden places and viewpoints. We will also go the observation tower in Pyynikki where you can enter the tower to see over the whole city. This tour takes about 2 ½ hours.  Normal price is EUR 30,-. Participants of the congress can join for EUR 25,- and if 6-12 persons from the congress are joining it will cost EUR 22,50- a person.</li>
<li><b>Bikerent</b>: Normal price for the biker ent is EUR 15,- for a day. Participants of the congress can rent a bike for EUR 12,50. These bikes have 21 gears and come with a lock and helmet.  More information about the bikerent can be found on <a href="http://www.eattampere.com/bikebicyclerenttampere.html" data-proofer-ignore>http://www.eattampere.com/bikebicyclerenttampere.html</a></li>
</ul>

You can book in advance, because we have limited amount of places available on the tours, because we want to offer personalized tours to our customers. *Bookings can be done by phone +358 (0)466863469 or by sending us a mail with the tour that you want to do and your contact details.*
