---
title:       "Previous Events"
date:        2010-04-07
menu:
  "2010":
    weight: 8
---
Akademy 2010 is the 8th edition of the KDE community summit. In 2009 Akademy was part of the Gran Canaria Desktop Summit. If you would like to find out more about the previous conferences, please visit the links provided here.

- [Akademy 2009](http://akademy2009.kde.org/)
- [Akademy 2008](http://akademy2008.kde.org/)
- [Akademy 2007](http://akademy2007.kde.org/)
- [Akademy 2006](http://akademy2006.kde.org/)
- [Akademy 2005](http://conference2005.kde.org/)
- [Akademy 2004](http://conference2004.kde.org/)
- <a href="http://events.kde.org/info/kastle/" data-proofer-ignore>Kastle 2003</a>
