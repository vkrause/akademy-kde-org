---
title:       "Social activities"
date:        2010-03-15
changed:     2010-06-18
menu:
  "2010":
    parent: social
    weight: 1
scssFiles:
- /scss/2010/images.scss
---
**Child pages**
- [Akademy Party](../akademyparty)
- [Sightseeing](../sights)
- [Day Trip](../daytrip)
- [Going Out](../goingout)

---

<p>Akademy wouldn't be Akademy without lots of possibilities for discussions, meeting old and new friends and fun activities. The traditional outing will take place on Thursday, 8 July. Details can be found <a href="../daytrip">here</a>.</p>
<p>If you would like to visit the sights in and around Tampere check out <a href="../sights">the local attractions</a>. Restaurants and pubs recommended by the local team can be found <a href="../goingout">here</a>.</p>

<p><img class="framed" style="display: block; margin-left: auto; margin-right: auto;" src="http://farm4.static.flickr.com/3471/3706079029_ef73728939.jpg" alt="" /></p>
