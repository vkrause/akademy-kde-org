---
title:       On why it is important to do self-marketing and why it's not "selling out"
date:        2010-05-05
changed:     2010-05-10


---
<p><strong>Speaker:</strong> Igor Schwarzmann</p>

<p>In a world full of iPhones and iPads, it is not enough to have the best code<br /><br />Changing the world one line of code at a time is a noble and very successful endeavour. Open Source changed the world in a profound way and KDE is a big part of that.<br /><br />But Open Source still has this image of being to geeky and nerdy. Most people lack the proper understanding behind the philosophy, effectiveness and quality of projects like KDE. In a way they are suffering from their own wrong perception of the Open Source world.<br /><br />This is not going to change by itself. Especially not in while Apple and other companies are presenting the effectiveness and the appeal of closed, tightly controlled environments.<br /><br />Unfortunately, it's also very common for the Open Source community to either neglect certain necessities in "doing good and talking about it" or even to perceive it as something that lessons the actually result of once achievements.<br /><br />I would argue, that to a certain point Open Source projects and leaders in those communities have the social responsibility to engage more in marketing the achievements of those projects and the fundamental ideas of Open Source. Writing the code and educating people about those ideas isn't mutual exclusive, it needs to be engaged more broadly and on platforms where companies like Apple has already proven themselves as successful.</p>
<h2>Igor Schwarzmann</h2>
<p><img style="float: right;" src="/media/2010/speakers/igor_t3n.jpg" alt="" width="133" height="200" />I'm a long time blogger and explaining technology induced social change (but most people just call it social media) at Ketchum Pleon. Due to the nature of my employer, I have the experience help big companies and organizations to gain the ability to help themselves.<br /><br />Some parts of my online presence: http://www.wiredvanity.com/, http://www.cognitivecities.com/, http://posterous.wiredvanity.com/, http://twitter.com/zeigor</p>
