---
title:       "MeeGo redefining the Linux desktop landscape"
date:        2010-05-10


---
<p><strong>Speaker:</strong> Valtteri Halla</p>

<p>MeeGo is a new software platform and open source project for creating devices and applications for mobile computing. This keynote will give an overview of MeeGo, its objectives and current status of the software and team today. MeeGo comprises the largest coordinated competence pool of Linux and open source "desktop" software development today. Impacts of MeeGo on the Linux desktop technology and market landscape is covered. MeeGo is an open community for the industry and individuals alike. Community involvement and KDE-MeeGo co-operation opportunities are discussed: what, how, examples, questions, opportunities, ideas.</p>
<h2>Valtteri Halla</h2>
<p><img style="float: right;" src="/media/2010/speakers/Valtteri.jpg" alt="" width="175" height="200" />- First name: Valtteri<br />- Last name: Halla<br />- Job: Director, Nokia MeeGo Software <br />- Organisation: MeeGo Devices / Nokia Devices / Nokia<br />- Other Roles: Member of Nokia CEO Technology Council, Co-chair of MeeGo Technical Steering Group<br />- Professional profile: Generalist in the fields of innovation, product and technology management. <br />- Education: M.Sc. Computer and Information Science (HUT), M.Sc. Economics (HSE)<br />- Work in last 10 years: Various roles on applying Open Source asset and method to create a mobile handheld computer product platform and products for Nokia - From research labs to productisation and commercial breakthrough<br />- Previous work: c.a. 5 years in various roles in software development and technology &amp; strategy management</p>
