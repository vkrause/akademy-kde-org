---
title:       "Tools and Tips for KDE development"
date:        2010-05-05
changed:     2016-11-02


---
<p><strong>Speaker:</strong> Thomas McGuire</p>

<p>There are a multitude of different tools available for developing for KDE, more than just a plain text editor and a compiler. Using these tools makes development more productive and often more fun as well. In this talk, I will show some of the tools I use for development, ranging from simple tools like GDB over valgrind to command line tools and IDEs such as KDevelop.<br />Many of these tools are already known to most KDE developers, yet there should be something new for everybody. I will present some advanced aspects of the tools, such as watchpoints, reverse debugging or pretty printing in GDB, or profiling your application with callgrind and massif, including visualization of the results with KCacheGrind and massif-visualizer. I will continue with showing some of the powerful features of modern IDEs, like signal/slot code completion or code navigation.<br />In addition, I will present useful command line tips, like the autojump script, the cgdb frontend for GDB or the tig frontend for git, and how to make building with a lot faster by using the /fast targets of CMake.<br /><br />Most of the talk will be showing things in action, and not showing slides.<br /><br />At the end of the talk, I would like to hear from you what you use to increase your productivity for KDE development.</p>
<h2>Thomas McGuire</h2>
<p><img style="float: right;" src="/media/2010/speakers/thomasmcguire.jpeg" alt="" width="200" height="200" /></p>
