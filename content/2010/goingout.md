---
title:       "Recommended Restaurants and Pubs"
date:        2010-03-16
changed:     2010-07-05
menu:
  "2010":
    parent: social
    name: Going Out
    weight: 5
scssFiles:
- /scss/tables.scss
- /scss/2010/table_sorted.scss
jsFiles:
- /js/2010/tablesort.js
---

<h2>Restaurants</h2>
<p>The following table will give you a short overview of available restaurants their type, location and pricing. <br /><em>*Click on a column header to sort.<br /></em></p>

<table class="stretch sorted" onclick="sortColumn(event)">
<thead> 
<tr>
<td>Restaurant</td>
<td>Location</td>
<td>Type</td>
<td>Pricing</td>
</tr>
</thead> 
<tbody>
<tr>
<td><strong>Golden Dragon</strong></td>
<td>Itsenäisyydenkatu 2</td>
<td>Chinese</td>
<td>5-10 EUR</td>
</tr>
<tr>
<td><strong>Sevilla</strong></td>
<td>Itsenäisyydenkatu 2</td>
<td>Spanish</td>
<td>10-25 EUR</td>
</tr>
<tr>
<td><strong>Lempi</strong></td>
<td>Itsenäisyydenkatu 5</td>
<td>Turkish</td>
<td>5-7 EUR</td>
</tr>
<tr>
<td><strong>La Toore</strong></td>
<td>Itsenäisyydenkatu 7-9</td>
<td>Turkish and pizza</td>
<td>5-10 EUR</td>
</tr>
<tr>
<td><strong>Hesburger</strong></td>
<td>Itsenäisyydenkatu 7-9</td>
<td>Fast food burgers</td>
<td>5-7 EUR</td>
</tr>
<tr>
<td><strong><br /></strong></td>
<td>Tammelan puistokatu 34</td>
<td><br /></td>
<td><br /></td>
</tr>
<tr>
<td><strong>Pancho Villa</strong></td>
<td>Satakunnankatu 22</td>
<td>Mexican</td>
<td>10-20 EUR</td>
</tr>
<tr>
<td><strong>Opera Brasserie</strong></td>
<td>Yliopistonkatu 44</td>
<td>French</td>
<td>15-20 EUR</td>
</tr>
<tr>
<td><strong>Ravintola Myllärit</strong></td>
<td>Åkerlundinkatu 4</td>
<td>French</td>
<td>20-25 EUR</td>
</tr>
<tr>
<td><strong>Eetvartti</strong></td>
<td>Sumeliuksenkatu 16</td>
<td>Lunch menu</td>
<td>5-10 EUR</td>
</tr>
<tr>
<td><strong>American Diner</strong></td>
<td>Itäinenkatu 9-13</td>
<td>Burger restaurant</td>
<td>10-20 EUR</td>
</tr>
<tr>
<td><strong>Bella Roma</strong></td>
<td>Itäinenkatu 5-7</td>
<td>Italian</td>
<td>10-20 EUR</td>
</tr>
<tr>
<td><strong>The Grill</strong></td>
<td>Freckellin aukio, Finlayson area</td>
<td>Grilled meat and fish specialities</td>
<td>15-30 EUR</td>
</tr>
<tr>
<td><strong>Speakeasy</strong></td>
<td>Päämääränkuja 10, Finlayson area</td>
<td>American Diner</td>
<td>5-15 EUR</td>
</tr>
<tr>
<td><strong>Ravintola Selvi14</strong></td>
<td>Satakunnankatu 18A, Finlayson area</td>
<td>Lunch buffet, falafel, kebab, pizzas</td>
<td>5-10 EUR</td>
</tr>
<tr>
<td><strong>Tivoli smørrebrød</strong></td>
<td>Itäinenkatu 9-13, Finlayson area</td>
<td>Sandwiches and other snacks</td>
<td>5-15 EUR</td>
</tr>
<tr>
<td><strong>Gopal</strong></td>
<td>Ilmarinkatu 16</td>
<td>Vegetarian <em>open only between 12-16</em></td>
<td>5-10 EUR</td>
</tr>
<tr>
<td><strong>Nanda Devi</strong></td>
<td>Näsilinnankatu 17</td>
<td>Indian</td>
<td>8,50-20 EUR</td>
</tr>
<tr>
<td><strong>Knossos</strong></td>
<td>Hatanpäänvaltatie 1</td>
<td>Greek</td>
<td>6,50-20 EUR</td>
</tr>
<tr>
<td><strong>Fuudis</strong></td>
<td>Puutarhakatu 12</td>
<td>Wok, vegetarian and vegan</td>
<td>4-10 EUR</td>
</tr>
</tbody>
</table>
<h2>Pubs and Bars</h2>
<p>The following table will give you a short overview of available pubs and bars, their type, location and pricing.<br /><em>*Click on a column header to sort.</em></p>
<table class="stretch sorted" onclick="sortColumn(event)">
<thead> 
<tr>
<td>Pub/Bar</td>
<td>Location</td>
<td>Type</td>
<td>Pricing</td>
</tr>
</thead> 
<tbody>
<tr>
<td><strong>Ruma</strong></td>
<td>Murtokatu 1</td>
<td>Rock bar, also serves snacks</td>
<td>5-7 EUR</td>
</tr>
<tr>
<td><strong>Klubi</strong></td>
<td>ullikamarin aukio 2</td>
<td>Music club, also serves snacks and has a lunch menu</td>
<td>5-10 EUR</td>
</tr>
<tr>
<td><strong>Telakka</strong></td>
<td>Tullikamarin aukio 3</td>
<td>Music club and restaurant</td>
<td>5-7 EUR</td>
</tr>
<tr>
<td><strong>Gastropub Praha</strong></td>
<td>Itsenäisyydenkatu 11</td>
<td>Pub that serves various dishes and snacks</td>
<td>7-10 EUR</td>
</tr>
<tr>
<td><strong>Plevna</strong></td>
<td>Itäinenkatu 8, Finlayson area</td>
<td>Brewery Pub &amp; Restaurant</td>
<td>10-20 EUR</td>
</tr>
</tbody>
</table>
