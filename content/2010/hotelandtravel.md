---
title:       "Hotel & Travel"
date:        2010-03-15
changed:     2010-06-29
menu:
  "2010":
    parent: travel
    weight: 1
---
**Child pages**
- [Accommodation](../accommodation)
- [Travel](../travel)

---

<p>Akademy 2010 is located at <a href="http://en.wikipedia.org/wiki/Tampere">Tampere, Finland</a>. Tampere is the third largest city in Finland and the largest inland centre in the Nordic countries. Currently there are over 200,000 inhabitants in Tampere, and almost 300,000 inhabitants in Tampere Sub-Region, which comprises Tampere and its neighbouring municipalities.</p>
<p>Tampere’s city centre is surrounded by lake and ridge scenery, sited on an isthmus between lakes Pyhäjärvi and Näsijärvi. The Tammerkoski rapids run through the city.</p>

<p>Today, the city is best known for its high tech and extensive know-how in various fields. Expertise stems from fruitful co-operation between local enterprises and universities and has resulted in internationally renowned products.</p>
<p><strong>Akademy </strong>itself will take place at the <a href="http://www.uta.fi/english/">University of Tampere</a> and at <a href="http://www.demola.fi/">Demola</a>. For more details, please see <a href="../venue">Venue</a>.</p>
<p>For information on hotels and hostels with special Akademy rates, please see <a href="../accommodation">Accommodation</a>.</p>
<p>Information about the nearest airports and public transport from Helsinki to Tampere can be found on <a href="../travel">Travel</a> page and in the <a href="http://community.kde.org/Events/Akademy/2010/TampereInfo">KDE community wiki</a>.</p>
<p>If you need to apply for a <strong>visa</strong> to travel to Finland, please contact the Akademy team at akademy-team at kde dot org for invitation letters and other visa requirements. Please provide information on your previous involvement with KDE or why you wish to attend the event.</p>
