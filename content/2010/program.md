---
title:       "Program"
date:        2010-03-12
changed:     2016-11-02
menu:
  "2010":
    parent: program
    weight: 1
scssFiles:
- /scss/tables.scss
---
**Child pages**
- [Call for Papers](../papers)
- [Presentations](./conference)
- [KDE e.V. AGM](../kdeevagm)
- [Hands-on Qt for mobile](./meetnokiaqt)

---

## Schedule Overview
Akademy 2010 officially starts on Saturday, 3 July. Registration/check-in at the university will be open from 8.30 am on. Please make sure you arrive early to be able to register before the official opening of Akademy starts at 9.30 am.

If you arrive on Friday, 2 July, during the day you will be able to check in to the  conference early. This pre-registration will take place at the <a href="../venue">Demola in the Finlayson area</a>. Pre-registration will be from 4 pm to 7 pm.

We also invite you to the welcome reception taking place at Demola on  Friday evening from 6 pm on.

<table class="stretch">
<tbody>
<tr>
<th style="text-align: center;">Day</th><th style="text-align: center;">Morning</th><th style="text-align: center;">Afternoon</th><th style="text-align: center;">Evening</th>
</tr>
<tr>
<td>Fri, 2 July</td>
<td>&nbsp;</td>
<td class="highlight" style="text-align: center;" colspan="2">Arriving, pre-registration, welcome drinks at Demola</td>
</tr>
<tr>
<td>Sat, 3 July</td>
<td class="highlight" style="text-align: center;" colspan="2"><a href="../program/conference#sched_sat">Akademy conference</a></td>
<td class="highlight" style="text-align: center;"><a href="../akademyparty">Akademy party</a></td>
</tr>
<tr>
<td>Sun, 4 July</td>
<td class="highlight" style="text-align: center;" colspan="2"><a href="../program/conference#sched_sun">Akademy conference</a></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Mon, 5 July</td>
<td class="highlight" style="text-align: center;" colspan="2"><a href="http://ev.kde.org/generalassembly/">KDE e.V. AGM</a>, <a href="../program/meetnokiaqt">Hands-on Qt for mobile</a></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Tue, 6 July</td>
<td class="highlight" style="text-align: center;" colspan="2"><a href="http://community.kde.org/Events/Akademy/2010/Tuesday">Workshops and BoFs</a></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Wed, 7 July</td>
<td class="highlight" style="text-align: center;" colspan="2"><a href="http://community.kde.org/Events/Akademy/2010/Wednesday">Workshops and BoFs</a></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Thu, 8 July</td>
<td class="highlight" style="text-align: center;"><a href="http://community.kde.org/Events/Akademy/2010/Thursday">Workshops</a></td>
<td class="highlight" style="text-align: center;" colspan="2"><a href="../daytrip">Day trip</a></td>
</tr>
<tr>
<td>Fri, 9 July</td>
<td class="highlight" style="text-align: center;" colspan="2"><a href="http://community.kde.org/Events/Akademy/2010/Friday">Workshops and BoFs</a></td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
