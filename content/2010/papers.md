---
title:       'Call for Papers - Akademy 2010: "Our World, Clearly"'
date:        2010-03-16
changed:     2010-04-14
menu:
  "2010":
    parent: program
    name: Call for Papers
    weight: 2
---
<img class="img-fluid mb-3" src="/media/2010/presentations.page_.png" alt="" align="center" />

<p>Akademy is the annual conference of the KDE community and open to all who share an interest in the KDE community and its goals. This conference brings together artists, designers, programmers, translators, users, writers and other contributors to celebrate the achievements of the past year. Moreover, at Akademy visions for the next year are defined. We invite all contributors and users to participate in Akademy in Tampere, Finland from July 3 to 10 2010.</p>

<p>Recent developments in the KDE technologies make it possible for users to connect with their data and with other users in new ways. In order to build upon this work, the main topics for this year's Akademy will be:</p>
<ul>
<li>
<p><strong style="font-weight: bold;">Expanding Our World: KDE Beyond The Linux Desktop</strong></p>
<p>KDE technologies have become increasingly available on mobile devices such as netbooks and mobile phones. This brings new challenges to the development of the KDE applications as well as new oportunities to greatly increase the visibility of the KDE community.</p>
<p>For this topic we encourage talks addressing the challenges of porting the KDE platform and applications to different operating systems and mobile devices, such as mobile phones and netbooks.</p>
</li>
<li>
<p><strong style="font-weight: bold;">Connecting Our World: Social Desktop</strong></p>
<p>Social desktop technology is allowing users of KDE applications to connect with each other and to have stronger links with the developer community. This technology brings a new and unique element to the KDE user experience.</p>
<p>For this topic we are requesting talks on the integration of KDE applications with social networks such as Facebook, identi.ca/Twitter or ownCloud. Talks regarding underlying services such as Akonadi and Telepathy are also encouraged.</p>
</li>
</ul>
<p>We also welcome submissions on any other KDE-related topic. This year we are requesting fewer, longer presentations in order to ensure the highest quality of content. Submissions can be made for either a 30 minute presentation or, following last year's success, a 45 minute presentation with a Technical paper. Such papers are to be submitted in the ACM format and should be between 4 and 6 pages long. Abstracts for the Technical paper should be included in the initial submission.</p>
<p>We are also accepting proposals for BoF and Workshop sessions that can last from one hour to all day. Topics can include anything relevant or of interest to the KDE community.</p>
<p>Submissions may be made by email to <a href="mailto:akademy-talks@kde.org">akademy-talks@kde.org</a>. They should include your name, small photo, talk/session title and an abstract of no more than 400 words. It should also indicate whether a 30 minute talk or a 45 minutes talk with a paper is being submitted.</p>
<p>The deadline for all submissions is <strong style="font-weight: bold;">Friday, 23 April 2010</strong>.</p>
