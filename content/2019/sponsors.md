---
title: Sponsors
menu:
  "2019":
    weight: 6
---
<h2>Sponsorship Opportunities</h2>
<p><strong>A big thank you to our sponsors who help to make this event happen! There are still sponsorship opportunities.<br>
If you are interested, please see <a href="/2019/sponsoring">Sponsoring Akademy 2019</a> for more information, including valuable sponsor benefits.</strong></p>
<h2>Sponsors for Akademy 2019</h2>
<table>
<tbody><tr>
<td colspan="2">
<h3>Platinum</h3>
</td>
</tr>
<tr>
<td><a name="tqtc" id="tqtc"></a><strong>The Qt Company</strong> (NASDAQ
 OMX Helsinki: QTCOM) is responsible for Qt development, productization 
and licensing under commercial and open-source licenses. Qt is a C++ 
based framework of libraries and tools that enables the development of 
powerful, interactive and cross-platform applications and devices.
<p>Used by over a million developers worldwide, Qt is a C++ based 
framework of libraries and tools that enables the development of 
powerful, interactive and cross-platform applications and devices. Qt’s 
support for multiple desktop, embedded and mobile operating systems 
allows developers to save significant time related to application and 
device development by simply reusing one code. Qt and KDE have a long 
history together, something The Qt Company values and appreciates. Code 
less. Create more. Deploy everywhere. <a href="https://www.qt.io/">https://www.qt.io/</a></p>
<p>The Future is Written with Qt</p></td>
<td style="width:275px;padding-left:50px;"><img class="img-fluid" src="/media/2018/tqtc_0.png"></td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td colspan="2">
<h3>Gold</h3>
</td>
</tr>
<tr>
<td><a name="bluesystems" id="bluesystems"></a><strong>Blue Systems</strong>
 is a company investing in Free/Libre Computer Technologies. It sponsors
 several KDE projects and distributions like Netrunner. Their goal is to
 offer solutions for people valuing freedom and choice.</td>
<td style="width:275px;padding-left:50px;"><img class="img-fluid" src="/media/2019/bluesystems2.png"></td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td colspan="2">
<h3>Silver</h3>
</td>
</tr>
<tr>
<td><a name="canonical" id="canonical"></a><strong>Canonical</strong> is
 the company behind Ubuntu, the leading OS for cloud operations. Most 
public cloud workloads use Ubuntu, as do most new smart gateways, 
switches, self-driving cars and advanced robots. Canonical provides 
enterprise support and services for commercial users of Ubuntu. 
Established in 2004, Canonical is a privately held company.  <a href="https://www.canonical.com/">https://www.canonical.com/</a></td>
<td style="width:275px;padding-left:50px;"><img class="img-fluid" src="/media/2018/ubuntu_transparent.png"></td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td colspan="2">
<h3>Bronze</h3>
</td>
</tr>
<tr>
<td><a name="kdab" id="kdab"></a><strong>KDAB</strong> is the world's 
leading software consultancy for architecture, development and design of
 Qt, C++ and OpenGL applications across desktop, embedded and mobile 
platforms. The biggest independent contributor to Qt, KDAB experts build
 run-times, mix native and web technologies, solve hardware stack 
performance issues and porting problems for hundreds of customers, many 
among the Fortune 500. KDAB’s tools and extensive experience in 
creating, debugging, profiling and porting complex, great looking 
applications help developers worldwide to deliver successful projects. 
KDAB’s global experts, all full-time developers, provide market leading 
training with hands-on exercises for Qt, OpenGL and modern C++ in 
multiple languages. <a href="https://www.kdab.com/">https://www.kdab.com/</a></td>
<td style="width:275px;padding-left:50px;"><img class="img-fluid" src="/media/2019/kdab2.png"></td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td><a name="opensuse" id="opensuse"></a><strong>The openSUSE project</strong>
 is a worldwide effort that promotes the use of Linux everywhere. 
openSUSE creates one of the world's best Linux distributions, working 
together in an open, transparent and friendly manner as part of the 
worldwide Free and Open Source Software community. The project is 
controlled by its community and relies on the contributions of 
individuals, working as testers, writers, translators, usability 
experts, artists and ambassadors or developers. The project embraces a 
wide variety of technology, people with different levels of expertise, 
speaking different languages and having different cultural backgrounds. 
Learn more at <a href="https://www.opensuse.org/">https://www.opensuse.org/</a></td>
<td style="width:275px;padding-left:50px;"><img class="img-fluid" src="/media/2015/opensuse.png"></td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td><a name="gitlab" id="gitlab"></a><strong>GitLab</strong> is a DevOps
 platform built from the ground up as a single application for all 
stages of the DevOps lifecycle enabling Product, Development, QA, 
Security, and Operations teams to work concurrently on the same project.
 GitLab provides teams a single data store, one user interface, and one 
permission model across the DevOps lifecycle allowing teams to 
collaborate and work on a project from a single conversation, 
significantly reducing cycle time and focus exclusively on building 
great software quickly. Built on Open Source, GitLab leverages the 
community contributions of thousands of developers and millions of users
 to continuously deliver new DevOps innovations. More than 100,000 
organizations from startups to global enterprise organizations, 
including Ticketmaster, Jaguar Land Rover, NASDAQ, Dish Network and 
Comcast trust GitLab to deliver great software at new speeds.</td>
<td style="width:275px;padding-left:50px;"><img class="img-fluid" src="/media/2019/gitlab.png"></td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td colspan="2">
<h3>Supporters</h3>
</td>
</tr>
<tr>
<td><a name="codethink" id="codethink"></a>In <strong>Codethink</strong>
 we specialise in system-level Open Source software infrastructure to 
support advanced technical applications, working across a range of 
industries including finance, automotive, medical, telecoms. Typically 
we get involved in software architecture, design, development, 
integration, debugging and improvement on the deep scary plumbing code 
that makes most folks run away screaming. <a href="https://www.codethink.co.uk/">https://www.codethink.co.uk</a></td>
<td style="width:275px;padding-left:50px;"><img class="img-fluid" src="/media/2018/codethink.png"></td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td><a name="slimbook" id="slimbook"></a><strong>Slimbook</strong> is a 
computer brand from Spain with Free Software vocation. Our products have
 consistently been designed and tested to run GNU/Linux and, through 
cooperation, provided among the best experience we can find in the 
market. In this fashion, we collaborated with KDE and created the KDE 
Slimbook laptop to make sure we have a good laptop that is today an 
example of what we can achieve working together. See you at Akademy! <a href="https://slimbook.es/">https://slimbook.es/</a></td>
<td style="width:275px;padding-left:50px;"><img class="img-fluid" src="/media/2018/slimbook.png"></td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td><a name="pine64" id="pine64"></a><strong>PINE64</strong> is a 
community driven project striving to deliver open source ARM64 devices 
to tinkerers, developers as well as education and business sectors. 
PINE64’s history began with a successful Kickstarter campaign 2015 of 
its original PINE A64(+) single board computer. The project has since 
grown in both scope and scale, and this year will introduce the Pinebook
 Pro, an ARM daily-driver laptop, as well as the PinePhone - a 
Linux-only smartphone. The core purpose of PINE64 is to provide the 
community with affordable and well-documented devices that everyone can 
use, develop for and apply in a multitude of scenarios <a href="https://pine64.org/">https://pine64.org/</a></td>
<td style="width:275px;padding-left:50px;"><img class="img-fluid" src="/media/2018/pine.png"></td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td><a name="oin" id="oin"></a><strong>Open Invention Network</strong> 
is the largest patent non-aggression community in history and supports 
freedom of action in Linux as a key element of open source software. 
Launched in 2005 and funded by Google, IBM, NEC, Philips, Red Hat, Sony,
 SUSE, and Toyota, OIN has more than 2,000 community members and owns 
more than 1,100 global patents and applications. The OIN patent license 
and member cross-licenses are available royalty free to any party that 
joins the OIN community. Joining OIN helps provide community members 
with patent freedom of action in Linux and adjacent open source 
technologies, free of charge or royalties.</td>
<td style="width:275px;padding-left:50px;"><img class="img-fluid" src="/media/2019/openinventionnetwork.svg"></td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td><a name="mycroft" id="mycroft"></a><strong>Mycroft</strong> The 
world's first Open Source Voice Assistant. We're building a voice AI 
that runs anywhere and interacts exactly like a person. Mycroft has 
products for developers, consumers, and enterprises who demand their 
technology be Private, Customizable, and Open. <a href="https://mycroft.ai/">https://mycroft.ai/</a></td>
<td style="width:275px;padding-left:50px;"><img class="img-fluid" src="/media/2018/mycroft_0.png"></td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td><a name="mbiton" id="mbiton"></a><strong>MBition</strong> is a 100% 
owned subsidiary of Daimler AG and focuses on Infotainment-Software, 
Navigation-Software, Cloud-Software and UI-Software. By combining the 
spirit and flexibility of a startup and the resources of a global active
 OEM, MBition develops advanced digital solutions. MBition's innovative 
products lead to disruptive changes in the automotive industry! <a href="https://mbition.io/en/home/">https://mbition.io/en/home/</a></td>
<td style="width:275px;padding-left:50px;"><img class="img-fluid" src="/media/2019/mbition.png"></td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td colspan="2">
<h3>VPN Provider</h3>
</td>
</tr>
<tr>
<td><a name="pia" id="pia"></a><strong>Private Internet Access (PIA)</strong>
 is the leading no-log VPN service provider in the world. PIA believes 
that access to an open internet is a fundamental human right and donates
 effusively to causes such as EFF, ORG, and FFTF to promote privacy 
causes internationally. PIA has over 3,200 servers in 25 countries that 
provide reliable, encrypted VPN tunnel gateways for whatever the use 
case <a href="https://www.privateinternetaccess.com/">https://www.privateinternetaccess.com/</a></td>
<td style="width:275px;padding-left:50px;"><img class="img-fluid" src="/media/2019/pia.png"></td>
</tr>
<tr>
<td colspan="2">
<h3>Hosted by</h3>
</td>
</tr>
<tr>
<td><a name="unimib" id="unimib"></a><strong>University of Milano-Bicocca</strong><br><a href="https://www.unimib.it/">https://www.unimib.it</a></td>
<td style="width:275px;padding-left:50px;"><img class="img-fluid" src="/media/2019/unimib.png"></td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td><a name="unixmib" id="unixmib"></a><strong>unixMiB</strong><br><a href="https://github.com/unixMiB">https://github.com/unixMiB</a></td>
<td style="width:275px;padding-left:50px;"><img class="img-fluid" src="/media/2019/unixmib.png"></td>
</tr>
<tr>
<td colspan="2">&nbsp;</td>
</tr>
<tr>
<td colspan="2">
<h3>Patrons of KDE</h3>
</td>
</tr>
<tr>
<td colspan="2"><a href="http://ev.kde.org/supporting-members.php">KDE's Patrons</a> also support the KDE Community through out the year</td>
</tr>
</tbody></table>
