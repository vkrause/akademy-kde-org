---
title:       "Program"
date:        2012-05-31
changed:     2016-11-02
menu:
  "2012":
    parent: program
    weight: 1
scssFiles:
- /scss/tables.scss
---
Akademy 2012 officially started on Saturday, 30th June. The overall schedule is shown below. <a href="https://www.google.com/calendar/ical/m0s6kg5qhvk84j4ctgjunob3vc%40group.calendar.google.com/public/basic.ics">Schedule in ical format</a>

<table class="stretch">
<tbody>
<tr>
<th style="text-align: center;">Day</th><th style="text-align: center;">Morning</th><th style="text-align: center;">Afternoon</th><th style="text-align: center;">Evening</th>
</tr>
<tr>
<td>Fri, 29 June</td>
<td>&nbsp;</td>
<td class="highlight" style="text-align: center;" colspan="2"><a href="../pre-registration-event">Arriving, pre-registration, get acquainted event</a></td>
</tr>
<tr>
<td>Sat, 30 June</td>
<td class="highlight" style="text-align: center;" colspan="2"><a href="#saturday">Akademy conference</a></td>
<td class="highlight" style="text-align: center;"><a href="http://www.rockcafe.ee/">Social event@Rock Cafe</a></td>
</tr>
<tr>
<td>Sun, 1 July</td>
<td class="highlight" style="text-align: center;" colspan="2"><a href="#sunday">Akademy conference</a></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Mon, 2 July</td>
<td class="highlight" style="text-align: center;" colspan="2"><a href="http://community.kde.org/Akademy/2012/Monday">Workshops and BoFs</a></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Tue, 3 July</td>
<td class="highlight" style="text-align: center;" colspan="2">KDE e.V. AGM</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Wed, 4 July</td>
<td class="highlight" style="text-align: center;" colspan="2"><a href="http://community.kde.org/Akademy/2012/Wednesday">Workshops and BoFs</a></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Thu, 5 July</td>
<td class="highlight" style="text-align: center;" colspan="2"><a href="http://community.kde.org/Akademy/2012/Thursday">Workshops and BoFs</a></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Fri, 6 July</td>
<td class="highlight" style="text-align: center;" colspan="2"><a href="http://community.kde.org/Akademy/2012/Friday">Workshops and BoFs</a></a></td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>

<br>

<table id="saturday" class="schedule">
<tbody>
<tr>
<th colspan="3" style="text-align: center;"><big><big>Saturday, 30 June 2012</big></big></th>
</tr>
<tr>
<th>Time</th><th>Room 1</th><th>Room 2</th>
</tr>
<tr>
<td>9:30 - 9:45</td>
<td colspan="2" class="notrack">Opening<br /></td>
</tr>
<tr>
<td>9:45 - 10:10</td>
<td class="track_keynote" colspan="2">Keynote: Mathias Klang<br /><a href="http://dot.kde.org/2012/04/22/akademy-keynote-dr-mathias-klang-freedom-expression">Freedom of Expression</a>  <a href="http://files.kde.org/akademy/2012/videos/Freedom_of_Expression_-_Mathias_Klang.m4v"><img src="/media/2012/video.png" /></a><br /></td>
</tr>
<tr>
<td>10:15 - 10:55</td>
<td><a href="/2012/node/23">Companies are Community—Meritocracy</a><br />
<em>Mirko Böhm</em> <a href="http://files.kde.org/akademy/2012/slides/Companies_are_Community_Meritocracy_-_Mirko_Bohm.pdf"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/Companies_are_Community_Meritocracy_-_Mirko_Bohm.m4v"><img src="/media/2012/video.png" /></a><br /></td>
<td>GSoC / SoK student presentation  <a href="http://files.kde.org/akademy/2012/slides/GSoC+SoK"><img src="/media/2012/slides.png" /></a> <a href="http://files.kde.org/akademy/2012/videos/GSoC_and_SoK_Student_Presentations.m4v"><img src="/media/2012/video.png" /></a><br /></td>
</tr>
<tr>
<td>10:55 - 11:05</td>
<td class="notrack" colspan="2">Break<br /></td>
</tr>
<tr>
<td>11:05 - 11:45</td>
<td><a href="/2012/node/33">KDE Releases That Just Work™</a><br /><em>Jeroen van Meeuwen</em> <a href="http://files.kde.org/akademy/2012/slides/KDE_Releases_That_Just_Work_-_Jeroen_van_Meeuwen.pdf"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/KDE_Releases_That_Just_Work_-_Jeroen_van_Meeuwen.m4v"><img src="/media/2012/video.png" /></a><br /></td>
<td><a href="/2012/node/32">KDE Frameworks 5: The Community Experiment</a><br /><em>Kévin Ottens</em> <a href="http://files.kde.org/akademy/2012/slides/KDE_Frameworks_5_The_Community_Experiment_-_Kevin_Ottens.svg"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/KDE_Frameworks_5_The_Community_Experiment_-_Kevin_Ottens.m4v"><img src="/media/2012/video.png" /></a><br /></td>
</tr>
<tr>
<td>11:50 - 12:30</td>
<td><a href="/2012/node/29">How to make your commit seen</a><br /><em>Marta Rybczyńska</em> <a href="http://files.kde.org/akademy/2012/slides/How_to_make_your_commit_seen_-_Marta_Rybczynska.pdf"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/How_to_make_your_commit_seen_-_Marta_Rybczynska.m4v"><img src="/media/2012/video.png" /></a><br /></td>
<td><a href="/2012/node/31">KDE Frameworks 5 for Application Developers</a><br /><em>David Faure</em> <a href="http://files.kde.org/akademy/2012/slides/KDE_Frameworks_5_for_Application_Developers_-_David_Faure.pdf"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/KDE_Frameworks_5_for_Application_Developers_-_David_Faure.m4v"><img src="/media/2012/video.png" /></a><br /></td>
</tr>
<tr>
<td>12:30 - 14:00</td>
<td class="notrack" colspan="2">Lunch<br /></td>
</tr>
<tr>
<td>14:00 - 14:25</td>
<td class="track_keynote" colspan="2">Community Keynote<br /><em>Agustin Benito Bethencourt</em>  <a href="http://files.kde.org/akademy/2012/slides/Community_Keynote_-_Agustin_Benito_Bethencourt.pdf"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/Community_Keynote_-_Agustin_Benito_Bethencourt.m4v"><img src="/media/2012/video.png" /></a><br /></td>
</tr>
<tr>
<td>14:30 - 15:10</td>
<td><a href="/2012/node/19">A New Hope: Open KDE Devices</a><br /><em>Aaron Seigo</em>  <a href="http://files.kde.org/akademy/2012/videos/A_New_Hope_Open_KDE_Devices_-_Aaron_Seigo.m4v"><img src="/media/2012/video.png" /></a><br /></td>
<td><a href="/2012/node/40">Qt Project Update</a><br /><em>Thiago Macieira</em> <a href="http://files.kde.org/akademy/2012/slides/Qt_Project_Update_-_Thiago_Macieira.pdf"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/Qt_Project_Update_-_Thiago_Macieira.m4v"><img src="/media/2012/video.png" /></a><br /></td>
</tr>
<tr>
<td>15:15 - 15:55</td>
<td><a href="/2012/node/37">Plasma Active—Freeing the Device Spectrum</a><br /><em>Sebastian Kügler</em> <a href="http://files.kde.org/akademy/2012/slides/Plasma_Active_Freeing_the_Device_Spectrum_-_Sebastian_Kugler.pdf"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/Plasma_Active_Freeing_the_Device_Spectrum_-_Sebastian_Kugler.m4v"><img src="/media/2012/video.png" /></a><br /></td>
<td><a href="/2012/node/39">Qt in the Nokia Strategy - an Update</a><br /><em>Quim Gil</em>  <a href="http://files.kde.org/akademy/2012/videos/Qt_in_the_Nokia_Strategy_an_Update_-_Quim_Gil.m4v"><img src="/media/2012/video.png" /></a><br /></td>
</tr>
<tr>
<td>16:00 - 16:40</td>
<td><a href="/2012/node/27">How Contour became Plasma Active: The next step of the mobile platform</a><br /><em>Eva Brucherseifer</em>  <a href="http://files.kde.org/akademy/2012/slides/How_Contour_became_Plasma_Active_The_next_step_of_the_mobile_platform_-_Eva_Brucherseifer.pdf"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/How_Contour_became_Plasma_Active_The_next_step_of_the_mobile_platform_-_Eva_Brucherseifer.m4v"><img src="/media/2012/video.png" /></a><br /></td>
<td><a href="/2012/node/38">Qt Components & Qt Styles in Qt 5</a><br /><em>Thiago Lacerda and Daker Fernandes Pinheiro</em>  <a href="http://files.kde.org/akademy/2012/slides/Qt_Components_and_Qt_Styles_in_Qt_5_-_Thiago_Lacerda_and_Daker_Fernandes_Pinheiro.tgz"><img src="/media/2012/slides.png" /></a> <a href="http://files.kde.org/akademy/2012/videos/Qt_Components_and_Qt_Styles_in_Qt_5_-_Thiago_Lacerda_and_Daker_Fernandes_Pinheiro.m4v"><img src="/media/2012/video.png" /></a><br /></td>
</tr>
<tr>
<td>16:40 - 16:55</td>
<td class="notrack" colspan="2">Break</td>
</tr>
<tr>
<td>16:55 - 17:35</td>
<td><a href="/2012/node/28">How the Plasma Active Human Interface Guidelines Makes Life Easier for Developers and Users</a><br /><em>Thomas Pfeiffer</em> <a href="http://files.kde.org/akademy/2012/slides/How_the_Plasma_Active_Human_Interface_Guidelines_Makes_Life_Easier_for_Developers_and_Users_-_Thomas_Pfeiffer.pdf"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/How_the_Plasma_Active_Human_Interface_Guidelines_Makes_Life_Easier_for_Developers_and_Users_-_Thomas_Pfeiffer.m4v"><img src="/media/2012/video.png" /></a><br /></td>
<td><a href="/2012/node/46">Wayland: Choosing a Better Input Method Architecture</a><br /><em>Michael Hasselmann</em> <a href="http://files.kde.org/akademy/2012/slides/Wayland_Choosing_a_Better_Input_Method_Architecture_-_Michael_Hasselmann.pdf"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/Wayland_Choosing_a_Better_Input_Method_Architecture_-_Michael_Hasselmann.m4v"><img src="/media/2012/video.png" /></a><br /></td>
</tr>
<tr>
<td>17:40 - 18:20</td>
<td><a href="/2012/node/43">The more we do the more there is to be done</a><br /><em>Nuno Pinheiro and Martin Zilz</em> <a href="http://files.kde.org/akademy/2012/slides/The_more_we_do_the_more_there_is_to_be_done_-_Nuno_Pinheiro_and_Martin_Zilz.pdf"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/The_more_we_do_the_more_there_is_to_be_done_-_Nuno_Pinheiro_and_Martin_Zilz.m4v"><img src="/media/2012/video.png" /></a><br /></td>
<td><a href="/2012/node/42">The future of packaging KDE SC Apps on Windows</a><br /><em>Patrick Spendrin and Patrick von Reth</em> <a href="http://files.kde.org/akademy/2012/slides/The_future_of_packaging_KDE_SC_Apps_on_Windows_-_Patrick_Spendrin_and_Patrick_von_Reth.pdf"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/The_future_of_packaging_KDE_SC_Apps_on_Windows_-_Patrick_Spendrin_and_Patrick_von_Reth.m4v"><img src="/media/2012/video.png" /></a><br /></td>
</tr>
</tbody>
</table>

<br>

<table id="sunday" class="schedule">
<tbody>
<tr>
<th colspan="3" style="text-align: center;"><big><big>Sunday, 1 July 2012</big></big></th>
</tr>
<tr>
<th>Time</th><th>Room 1</th><th>Room 2</th>
</tr>
<tr>
<td>9:30 - 9:55</td>
<td class="track_keynote" colspan="2">Keynote: Qt Project Achievements and How KDE Is Helping<br /><em>Thiago Macieira</em> <a href="http://files.kde.org/akademy/2012/slides/Qt_Project_Achievements_and_How_KDE_Is_Helping_-_Thiago_Macieira.pdf"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/Qt_Project_Achievements_and_How_KDE_Is_Helping_-_Thiago_Macieira.m4v"><img src="/media/2012/video.png" /></a><br /></td>
</tr>
<tr>
<td>10:00 - 10:40</td>
<td><a href="/2012/node/35">Making Multilingual Wikis a Reality</a><br /><em>Niklas Laxstöm and Claus Christensen</em> <a href="http://files.kde.org/akademy/2012/slides/Making_Multilingual_Wikis_a_Reality_-_Niklas_Laxstom_and_Claus_Christensen.html"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/Making_Multilingual_Wikis_a_Reality_-_Niklas_Laxstom_and_Claus_Christensen.m4v"><img src="/media/2012/video.png" /></a><br /></td>
<td><a href="/2012/node/25">Domain Specific Debugging Tools</a><br /><em>Volker Krause</em> <a href="http://files.kde.org/akademy/2012/slides/Domain_Specific_Debugging_Tools_-_Volker_Krause.pdf"><img src="/media/2012/slides.png" /></a>   <a href="http://files.kde.org/akademy/2012/videos/Domain_Specific_Debugging_Tools_-_Volker_Krause.m4v"><img src="/media/2012/video.png" /></a><br /></td>
</tr>
<tr>
<td>10:45 - 11:25</td>
<td><a href="/2012/node/34">Localizing Software for Multicultural Environments</a><br /><em>Runa Bhattacharjee</em> <a href="http://files.kde.org/akademy/2012/slides/Localizing_Software_for_Multicultural_Environments_-_Runa_Bhattacharjee.pdf"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/Localizing_Software_for_Multicultural_Environments_-_Runa_Bhattacharjee.m4v"><img src="/media/2012/video.png" /></a><br /></td>
<td><a href="/2012/node/26">Flexible Education with KDE</a><br /><em>Laszlo Papp and Aleix Pol Gonzalez</em> <a href="http://files.kde.org/akademy/2012/slides/Flexible_Education_with_KDE_-_Laszlo_Papp_and_Aleix_Pol_Gonzalez.pdf"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/Flexible_Education_with_KDE_-_Laszlo_Papp_and_Aleix_Pol_Gonzalez.m4v"><img src="/media/2012/video.png" /></a><br /></td>
</tr>
<tr>
<td>11:25 - 11:40</td>
<td class="notrack" colspan="2">Break</td>
</tr>
<tr>
<td>11:40 - 12:20</td>
<td class="tracklightning">Lightning Talks <a href="http://files.kde.org/akademy/2012/slides/Lightning_Talks"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/Lightning_Talks.m4v"><img src="/media/2012/video.png" /></a></td>
<td><a href="/2012/node/44">The Window Manager Construction Toolkit</a><br /><em>Martin Gräßlin</em> <a href="http://files.kde.org/akademy/2012/slides/The_Window_Manager_Construction_Toolkit_-_Martin_Grasslin.pdf"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/The_Window_Manager_Construction_Toolkit_-_Martin_Grasslin.m4v"><img src="/media/2012/video.png" /></a><br /></td>
</tr>
<tr>
<td>12:20 - 12:45</td>
<td class="notrack" colspan="2">Group Photo  <a href="https://devel-home.kde.org/~duffus/akademy/2012/groupphoto/"><img src="/media/2012/slides.png" /></a></td>
</tr>
<tr>
<td>12:45 - 14:00</td>
<td class="notrack" colspan="2">Lunch</td>
</tr>
<tr>
<td>14:00 - 14:25</td>
<td class="track_keynote" colspan="2">Keynote: Will Schroeder<br /><a href="http://dot.kde.org/2012/04/11/akademy-keynote-dr-will-schroeder-kitware-ceo">It has a heart, a brain and it's dangerous</a>  <a href="http://files.kde.org/akademy/2012/videos/It_has_a_heart_a_brain_and_its_dangerous_-_Will_Schroeder.m4v"><img src="/media/2012/video.png" /></a><br /></td>
</tr>
<tr>
<td>14:30 - 15:10</td>
<td><a href="/2012/node/36">Messaging for Free Software Groups and Projects</a><br /><em>Deb Nicholson</em> <a href="http://files.kde.org/akademy/2012/slides/Messaging_for_Free_Software_Groups_and_Projects_-_Deb_Nicholson.pdf"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/Messaging_for_Free_Software_Groups_and_Projects_-_Deb_Nicholson.m4v"><img src="/media/2012/video.png" /></a><br /></td>
<td><a href="/2012/node/56">Tine 2.0 and KDE - An Integration Initiative</a><br /><em>Cornelius Weiss</em> <a href="http://files.kde.org/akademy/2012/slides/Tine_2.0_and_KDE_An_Integration_Initiative_-_Cornelius_Weiss.pdf"><img src="/media/2012/slides.png" /></a> <a href="http://files.kde.org/akademy/2012/videos/Tine_2.0_and_KDE_An_Integration_Initiative_-_Cornelius_Weiss.m4v"><img src="/media/2012/video.png" /></a><br /></td>
</tr>
<tr>
<td>15:15 - 15:55</td>
<td><a href="/2012/node/24">Defensive Publications—Ensuring Freedom Of Use In The Open Source Community</a><br /><em>Raffi Gostanian</em>  <a href="http://files.kde.org/akademy/2012/videos/Defensive_Publications_Ensuring_Freedom_Of_Use_In_The_Open_Source_Community_-_Raffi_Gostanian.m4v"><img src="/media/2012/video.png" /></a><br /></td>
<td><a href="/2012/node/21">Building new KDE ecosystems: KDE Connect</a><br /><em>Agustín Benito Bethencourt</em> <a href="http://files.kde.org/akademy/2012/slides/Building_new_KDE_ecosystems_KDE_Connect_-_Agustín_Benito_Bethencourt.pdf"><img src="/media/2012/slides.png" /></a>  <!-- <a href="http://files.kde.org/akademy/2012/videos/Building_new_KDE_ecosystems_KDE_Connect_-_Agustín_Benito_Bethencourt.m4v"><img src="/media/2012/video.png" /></a>--><br /></td>
</tr>
<tr>
<td>15:55 - 16:10</td>
<td class="notrack" colspan="2">Break</td>
</tr>
<tr>
<td>16:10 - 16:50</td>
<td><a href="/2012/node/20">Accessibility</a><br /><em>Frederik Gladhorn</em> <a href="http://files.kde.org/akademy/2012/slides/Accessibility_-_Frederik_Gladhorn.pdf"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/Accessibility_-_Frederik_Gladhorn.m4v"><img src="/media/2012/video.png" /></a><br /></td>
<td><a href="/2012/node/30">KDE e.V.—Foundation for the Community</a><br /><em>Sebastian Kügler and Lydia Pintscher</em> <a href="http://files.kde.org/akademy/2012/slides/KDE_eV_Foundation_for_the_Community_-_Sebastian_Kugler_and_Lydia_Pintscher.pdf"><img src="/media/2012/slides.png" /></a> <a href="http://files.kde.org/akademy/2012/videos/KDE_eV_Foundation_for_the_Community_-_Sebastian_Kugler_and_Lydia_Pintscher.m4v"><img src="/media/2012/video.png" /></a><br /></td>
</tr>
<tr>
<td>16:55 - 17:35</td>
<td><a href="/2012/node/22">Building Accessible Systems: Accessibility in Practice</a><br /><em>Peter Grasch</em> <a href="http://files.kde.org/akademy/2012/slides/Building_Accessible_Systems_Accessibility_in_Practice_-_Peter_Grasch.svg"><img src="/media/2012/slides.png" /></a>  <a href="http://files.kde.org/akademy/2012/videos/Building_Accessible_Systems_Accessibility_in_Practice_-_Peter_Grasch.m4v"><img src="/media/2012/video.png" /></a><br /></td>
<td><a href="/2012/node/41">Open Source Computer Vision Technology usable by People with Physical Disabilities</a><br/><em>Akarsh Sanghi</em> <a href="http://files.kde.org/akademy/2012/slides/Open_Source_Computer_Vision_Technology_usable_by_People_with_Physical_Disabilities_-_Akarsh_Sanghi.pdf"><img src="/media/2012/slides.png" /></a> <!-- <a href="http://files.kde.org/akademy/2012/videos/Open_Source_Computer_Vision_Technology_usable_by_People_with_Physical_Disabilities_-_Akarsh_Sanghi.m4v"><img src="/media/2012/video.png" /></a>--><br /></td>
</tr>
<tr>
<td>17:40 - 18:10</td>
<td class="track_keynote" colspan="2">Sponsor Presentations <a href="http://files.kde.org/akademy/2012/slides/Sponsors_Presentations"><img src="/media/2012/slides.png" /></a> <a href="http://files.kde.org/akademy/2012/videos/Sponsor_Presentations.m4v"><img src="/media/2012/video.png" /></a></td>
</tr>
<tr>
<td>18:10 - 18:20</td>
<td class="track_keynote" colspan="2">Akademy Awards <a href="http://files.kde.org/akademy/2012/videos/Akademy_Awards.m4v"><img src="/media/2012/video.png" /></a></td>
</tr>
</tbody>
</table>

<br>

<em>Akademy 2012 Videos by the <a href="mailto:akademy-team@kde.org">Akademy Team</a> are licensed under a <a href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0 Unported License</a></em>
