---
title:       "Registration open for Workshop & BoF sessions"
date:        2012-06-05
---
The 4 days of Workshops & BoFs (Birds of a Feather sessions) are an important part of Akademy, where participants meet, discuss, work on projects and launch new initiatives. Coding sprints, working groups, and workshops bring together dedicated people to make surprising progress in a short amount of time.

<!--more-->

The <a href="http://community.kde.org/Akademy/2012#Workshops_and_BoFs">BoF and Workshop schedule</a> is available for registration. In the Unconference style, people select the times and locations that work best for their topic or group. Organization is done by participants; session leaders resolve schedule conflicts themselves. 

