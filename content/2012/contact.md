---
title:       "Contact"
date:        2012-01-30
changed:     2012-06-22
menu:
  "2012":
    weight: 7
---
Akademy 2012 is organized by <a href="http://ev.kde.org/">KDE e.V.</a> and <a href="http://www.alvatal.ee/">ALVATAL</a>.

To contact the Akademy organizing team please email <a href="mailto:akademy-team@kde.org">The Akademy Team</a>. For other matters, contact the <a href="mailto:kde-ev-board@kde.org">KDE e.V. Board</a>.

The IRC channel for the event is #akademy on Libera Chat, you are welcome to sit in the channel or just pop in and ask questions, but please remember you might not instantly get an answer so stay for a reasonable period.

During the event and the lead up if you need to contact the team by phone: [+372 56 044 752](tel:+372-56-044-752) or [+372 53 628 793](tel:+372-53-628-793). Please keep in mind that we are busy and only call if it is really urgent and this is the only way you can reach us.
