---
title:       "Call for Papers"
date:        2012-01-30
changed:     2012-02-12


---
The KDE community invites everyone to participate in an amazing Akademy 2012 in Tallinn, Estonia.

Akademy is the annual gathering of the KDE community and its friends. Akademy 2012 will highlight KDE's progress since its founding over 15 years ago. It will be a showcase for our innovative spirit and one of the largest and most dynamic open source communities in the world. It is an opportunity for KDE contributors to come together in person, and embrace new opportunities and partners such as the Qt Project.

We are asking for submissions on the following topics:

<ul>
<li>Maturity and Innovation</li>
<li>Embracing the Qt Project and other partners</li>
<li>Activities relevant to the KDE Frameworks efforts</li>
<li>Development of beautiful and performant applications</li>
<li>Making our software available to a wider audience through efforts such as accessibility efforts, promotion, translation and localization</li>
<li>Other topics that are relevant to KDE and the Qt ecosystem</li>
</ul>


Submissions should be <a href="mailto:akademy-talks@kde.org">emailed to the program committee</a> and include:
<ul>
<li>Name of the speaker(s) and a short bio paragraph</li>
<li>Title of the talk</li>
<li>One-paragraph description (~60 words)</li>
<li>Abstract (~250 words)</li>
<li>Length (5, 30 or 45 minutes)</li>
<li>Picture of the speaker(s)</li>
<li>Any special requirements (e.g., special video connectors, audio support, cameras)</li>
</ul>

Timeline:
<ul>
<li>Call for papers end: March 15th 2012</li>
<li>Notification of speakers: March 30th 2012</li>
<li>Akademy: June 30th to July 6th 2012</li>
</ul>

If you are interested in organizing a workshop session, a BoF or a project room, there will be a separate announcement in a few weeks.

The program committee (Kevin Krammer, Thiago Macieira, Celeste Lyn Paul, Lydia Pintscher and Aaron Seigo)
