---
title:       "Special Workshops, BoFs, Party And More"
date:        2012-06-13
---
Akademy is more than inspiring talks. It's also a place to plan, collaborate and get a lot of work done in <a href="http://dot.kde.org/2012/06/05/registration-opens-akademy-bofs-and-workshops">BoFs and workshops</a>.

This year, we have two special workshops that you won't want to miss.

<!--more-->

<h2>PR Workshop</h2>
On Monday, 2 July, there will be a <a href="http://community.kde.org/Akademy/2012/PRWorkshop">PR workshop for Free Software projects</a>. In this workshop, two professional journalists—Jake Edge from LWN and Markus Feilner from Linux Magazin Germany—will show you how to get the word out about your project, how to get attention in the marketplace and how to write an effective press release. This interactive, hands-on workshop will help you make the connection between your software and users who appreciate it.

<h2>Qt Quick 2</h2>
Also on Monday, you can dive into Qt5 with the Qt Quick 2 training. Qt5 is coming; Qt Quick is one of its outstanding features. This training will cover the basic concepts of Qt Quick 2. It will show you how to make your own GUI with an emphasis on sophisticated user interaction, animations and transitions between states. Learn how to use the Qt C++ API to extend the Qt Quick 2 runtime. The training will be conducted by <a href="http://www.kdab.com/">KDAB</a>, a world leader in Qt training and development. Ordinarily there is a charge for this training, but it is free at Akademy, thanks to Nokia’s sponsorship and their commitment to Qt and KDE. <a href="http://community.kde.org/Akademy/2012/QtQuick">Registration</a> is required prior to the workshop; seating is limited.

<h2>BoFs</h2>
There's more ... from Monday through Friday there are many BoF sessions. Topics already scheduled range from KDE QA to the FSFE PDF readers sprint to a session on how to author KDE books and a tutorial on Telepathy. Visit the <a href="http://community.kde.org/Akademy/2012#Workshops_and_BoFs">Akademy 2012 Wiki</a> to see the sessions that have already been scheduled. There is no better time or place than Akademy to get your own project up and running. Visit the <a href="http://community.kde.org/Akademy/2012#Workshops_and_BoFs">Akademy Wiki</a> to sign up for a time slot.

<h2>Relax, socialize, entertain</h2>
Akademy means intense work, but its parties are also well known. This year's main party is sponsored by Blue Systems and will take place on Saturday, 30 June at Rock Café in Tallinn. Your Akademy badge will get you in.

The party this year includes a special show—KDE's Got Talent! In previous years, there have been a few spontaneous outbreaks of creativity. This year there is an organized Open Mike for people to share their talents and have fun. The show depends on audience members to listen and perform … music, spoken word, comedy, dance. No matter your act or creative gift, you will not find a more appreciative audience than the KDE Community. Visit the <a href="http://community.kde.org/Akademy/2012/KGT">Akademy Wiki</a> to sign up. Join in. Life is about more than technology.
