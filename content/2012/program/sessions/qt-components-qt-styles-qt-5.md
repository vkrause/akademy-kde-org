---
title:       "Qt Components & Qt Styles in Qt 5"
date:        2012-05-10
changed:     2012-05-13
aliases:     [ /2012/node/38 ]

---
<strong>Speakers:</strong> Thiago Lacerda and Daker Fernandes Pinheiro

Since its introduction, QtQuick has taken a central role in interface development. Currently, there is not an independent set of QML widgets for speeding up the development of desktop applications.


There is another issue with using Qt abstractions when a developer wants to create widgets with the platform look and feel. At this time, the only way to do that using Qt is to use the old QtStyles API, depending completely on QtWidgets module. There is no way to create these widgets easily in QML without using QtWidgets, even though this capability has become critically important to developers.

Based on these issues and experience gained from developing Plasma components, Qt components for MeeGo and complex desktop applications using QML, we decided to develop a set of desktop components, targeting Qt 5 and QtQuick 2, independent of the QtWidgets module. The components will have the supporting platform look and feel by default. And developers will be able to customize the components by setting some parameters in either pure QML or C++.

With this new module, developers can accelerate the development phase by using our built-in QML components, getting rid of basic component development. With more developers using the QtQuick 2 framework with this new module, it can become an important part of the Qt framework, and thus part of the KDE desktop applications framework.

In this presentation, we will discuss the current problems that developers face when creating rich UI desktop applications, explore the benefits that our project will provide and how it can help to solve the current problems. We will also show the current state of the project and show some demos and examples of how application development can be easier.
<p>&nbsp; </p>
<h2>Thiago Lacerda, Daker Fernandes Pinheiro</h2>
Daker collaborates with Plasma/KDE and other Qt/QML/JS/Python/Prolog-related open source projects in his free time. He also has contributed to the Qt-Component projects for MeeGo and Plasma. He currently works as a Qt Framework and Application developer at Nokia Institute of Technology (INdT). He holds a diploma for Computer Science at Federal University of Pernambuco (Brazil), and lives in Recife, Brazil.

Thiago Lacerda graduated in Computer Science at Federal University of Pernambuco (Brazil), has been using Linux and KDE since 2005 and is passionate about software development. He worked for the Federal Univerity as a researcher and software engineer, at LG Electronics and is now part of the Qt Framework team at Nokia Institute of Technology (INdT), which has participated in the Qt-Component for MeeGo development and also on other desktop applications using Qt/QML.
