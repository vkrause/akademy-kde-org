---
title:       "Flexible Education with KDE"
date:        2012-05-10
changed:     2012-06-06
aliases:     [ /2012/node/26 ]

---
<strong>Speakers:</strong> Laszlo Papp and Aleix Pol Gonzalez

Ever since there was software, there has been a need for educational software. Unfortunately, there have not been many business use cases behind the KDE4 Educational Desktop for several reasons. The need for such a system is growing faster and faster. The KDE Edu project follows the widespread paradigm change that affects desktop and mobile usage. These changes are promising for KDE Edu because they open up new opportunities we have never imagined before.


There was a lot of effort put into making the educational stack a success on Nokia N9 devices, based on a vision created a year ago at a technically focused KDE Harmattan sprint. In addition, there are quite a few educational applications available in the Nokia Ovi Store.

Plasma Active could be an important addition to KDE Edu efforts. At a recent Plasma Active Three Sprint, the project vision and direction were discussed. The Android platform and supported devices can also play a substantial role in KDE's educational efforts; Android has an enormous and rapidly growing user base of kids, students, and teachers. There are new platforms such as the QNX operating system running on Playbook devices that can be expected to have implications for education.

Improved collaboration in an increasingly mobile world will benefit the educational users' experiences with closer relationships between end users and developers. Collaboration! That is what we need even more than ever.

The presentation will finish with a demo of existing solutions. Hope you will all enjoy it.
<p>&nbsp; </p>
<h2>Laszlo Papp, Aleix Pol Gonzalez</h2>
Laszlo Papp (djszapi) has a degree in electrical engineering. He is a Senior Software Engineer at Symbio Software Oy in Helsinki. He worked on the Maemo/MeeGo projects with a special interest in smart embedded systems. He has been a Qt/KDE enthusiast in his leisure time for a while, visiting open source Qt/KDE events and sprints.  He also likes reading interesting books and playing bass guitar.

Aleix Pol Gonzalez (apol) is from Barcelona and is employed by Blue Systems to work on KDE. Besides work-work, he likes working on KDE Edu and KDevelop. He is also involved in KDE España where he is the secretary and occasional speaker to
show random KDE goodies.
