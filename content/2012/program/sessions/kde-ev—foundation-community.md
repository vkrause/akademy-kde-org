---
title:       "KDE e.V.—Foundation for the Community"
date:        2012-05-10
changed:     2012-05-12
aliases:     [ /2012/node/30 ]

---
<strong>Speakers:</strong> Sebastian Kügler and Lydia Pintscher
KDE e.V. is the foundation that supports the KDE community organizationally, financially and if necessary, legally. Mostly acting in the background, it is not always the most visible organization, and that is by design. The KDE Community is not run by the KDE e.V., but rather is supported by it. KDE e.V. does not influence technical direction, but supports decisions made by the Community and enables the Community to grow and strive.


The KDE e.V. consists of hundreds of core KDE contributors. It was founded in 1997, shortly after the inception of the KDE Project. The KDE e.V. owns KDE's trademarks and other assets, but doesn't own KDE itself. So, what is the KDE e.V.? How does it operate? How is it run? How does it support the KDE Community? Who makes this possible? How is it financed? All these questions and more will be addressed in the presentation by Lydia Pintscher, who joined the KDE e.V. Board of Directors in 2011, and Sebastian Kügler, who has served the KDE e.V. as Director since 2006.

The presentation is for a general, non-technical audience mainly interested in community topics. The audience will get an inside look into how one of the largest Free software organizations is managed, how it grew over the past years, how to help support the foundation of our community, and how it can help anyone participate and do great things for KDE.
<p>&nbsp;</p>
<h2>Sebastian Kügler and Lydia Pintscher</h2>
Lydia Pintscher is a people geek and cat herder by nature. Among other things, she manages KDE's mentoring programs (Google Summer of Code, Google Code-in, Season of KDE), is a founding member of KDE's Community Working Group and is a board member of KDE e.V. In her day-job she does community communications for Wikidata at Wikimedia Germany.

Sebastian Kügler (sebas) has been hacking on KDE software since 2004. He is especially active in the Plasma team, but has contributed his fair share of code in other areas as well. Sebastian maintains a few of Plasma Desktop's default components, wrote others in the area of hardware integration and PIM. In 2011, Sebastian co-founded the Plasma Active project, and is one of its core developers. Since 2006, Sebastian serves the KDE community on the KDE e.V. Board of Directors in his current role as Vice President. Sebastian is employed by <a href="http://open-slx.com">open-slx</a>. In his free time, sebas enjoys photography, cooking, music and scuba diving.
