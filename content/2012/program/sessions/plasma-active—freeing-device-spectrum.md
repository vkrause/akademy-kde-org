---
title:       "Plasma Active—Freeing the Device Spectrum"
date:        2012-05-10
changed:     2012-05-13
aliases:     [ /2012/node/37 ]

---
<strong>Speaker:</strong> Sebastian Kügler

Plasma Active is a Free software system built to support a spectrum of devices. It is based on a well-known Free software stack, and aims at providing a user experience that aligns with how people integrate devices into their (digital) lives, rather than providing just a way to run applications.


Plasma Active provides more than just a launcher for applications. For users, it is a system that models around their digital lives. The concept of Activities organizes users' digital assets in a way that is not available on other mobile devices. For developers, Plasma Active enables a way to to get their software onto users' devices on the developers' own terms, not those dictated by large corporations. Most importantly, Plasma Active is a system open to anyone's imagination.

A bit more than one year ago, the KDE Community started the Plasma Active Project, a concerted and holistic effort to crack open the market for devices and provide a Free software system beyond the desktop. Much has happened in the first year. The team has released two stable versions of the system. Partnering with the <a href="http://merproject.org/">Mer Project</a> has led to a good number of supported devices. Even a tablet device has become available to customers around the world.


In his talk, Sebastian addresses a general audience and outlines concepts and achievements from the first year of Plasma Active, along with an outlook for what can be expected. Demoes round out this introduction.
<p>&nbsp; </p>
<h2>Sebastian Kügler</h2>
Sebastian Kügler (sebas) has been hacking on KDE software since 2004. He is especially active in the Plasma team, but has contributed his fair share of code in other areas as well. Sebastian maintains a few of Plasma Desktop's default components, wrote others in the area of hardware integration and PIM. In 2011, Sebastian co-founded the Plasma Active project, and is one of its core developers. Since 2006, Sebastian has served the KDE Community on the KDE e.V. Board of Directors. His current role is Vice President. Sebastian is employed by <a href="http://open-slx.com">open-slx</a>. In his Free time, sebas enjoys photography, cooking, music and scuba diving.
