---
title:       "Companies are Community—Meritocracy"
date:        2012-05-10
changed:     2012-05-21
aliases:     [ /2012/node/23 ]

---
<strong>Speaker:</strong> Mirko Böhm

For a long time, the KDE Community operated under very little influence from commercial and other external entities. Protection from undue influence by anyone is a defining constitutional principle of KDE e.V. The result is that of all Free Software communities, KDE is one of the few that has remained independent of major financial backers and—directly related—is able to steer a course of radical innovation, a course that would likely have been slowed down by the naturally more cautious institutional stakeholders. 


But this comes at a price. In mixed volunteer and commercial communities, companies and volunteers usually focus on different other tasks, and companies often provide the polish needed to woo end users. Recently with ownCloud, Vivaldi and others, commercial endeavors have been started that are based on KDE technology. Those are awesome initiatives, but to the surprise of some, they stirred a controversial reaction within the community. It is clear that there are benefits and opportunities in a growing network of companies investing in KDE technology. It is also clear that the current constitution of KDE and KDE e.V. does not provide such companies with a well-defined roadmap for how to become part of the Community on equitable terms.
 
This presentation will shed light on the why and how of KDE e.V., analyze and structure the potential benefits of commercial contributions to KDE, and conclude with what the KDE Connect program should keep in mind to improve the situation. It will look into an updated organizational KDE structure that allows commercial, government and other organizations to affect the influence due to them - not more, but also not less than an individual contributor. 
<p>&nbsp; </p>
<h2>Mirko Böhm</h2>
Mirko is a FLOSS advocate, primarily as a speaker and author. He has been a KDE contributor since 1997, including several years on the KDE e.V. Board. Currently, he is doing research on Open Source at the Technical University of Berlin. He's a member of the FSFE Germany team and a Qt-certified specialist and trainer. Mirko has a wide range of experience as an entrepreneur, KDAB manager, and a German Air Force officer. He lives with his wife and two kids in Berlin.
