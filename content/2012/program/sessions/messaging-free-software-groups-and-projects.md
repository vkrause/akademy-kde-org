---
title:       "Messaging for Free Software Groups and Projects"
date:        2012-05-10
changed:     2012-05-13
aliases:     [ /2012/node/36 ]

---
<strong>Speaker:</strong> Deb Nicholson

This talk is for people who want to refine their message and more effectively share their excitement about free software. It's easy to talk with people who already "get it", but how does someone start a conversation about free software with someone else who's never heard of it?


Messaging starts with relatively easy stuff like an elevator pitch and an FAQ. It becomes more advanced with issues such as strategically dealing with negative stereotypes and soliciting constant meaningful feedback. What "messages" are already being sent about the group or project? How can it be improved?  

I will discuss how to meet people part way, the use of inclusive terms and images, and how to understand selected audiences. Many aspects contribute to the image conveyed by a project or group, which are usually not deliberate or well-managed. I will provide simple strategies that can be employed right away, and bigger ideas for making messaging successful in the long-term. This talk is for anyone who wants to make sure that what's awesome about their project is shining through to existing users, potential new users and contributors.
<p>&nbsp; </p>
<h2>Deb Nicholson</h2>
Deb Nicholson works at the intersection of technology and social justice. She has been a free speech advocate, economic justice organizer and civil liberties defender. After working in Massachusetts politics for fifteen years, she became involved in the free software movement. She is the Community Outreach Director at the <a href="http://www.openinventionnetwork.com/">Open Invention Network</a> and the Community Manager at <a href="http://mediagoblin.org/">Media Goblin</a>. She also serves on the Board at <a href="http://openhatch.org/">Open Hatch</a>, a non-profit dedicated to matching prospective free software contributors with communities, tools and education. She lives in Cambridge, Massachusetts.
