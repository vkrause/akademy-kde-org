---
title:       "KDE Frameworks 5 for Application Developers"
date:        2012-05-10
changed:     2012-05-12
aliases:     [ /2012/node/31 ]

---
<strong>Speaker:</strong> David Faure

The upcoming KDE Frameworks 5 will replace kdelibs. In this presentation, application developers will learn what this means for them—functionality that has moved to Qt 5, frameworks that will be available on top, how to port existing KDE applications, and the benefits KDE Frameworks 5 will have for non-KDE Qt applications.


Since the Randa meeting in 2011, the kdelibs developers have taken steps to make kdelibs more modular and reusable. The main goal has been to reduce dependencies and overhead from unrelated features and classes (in the worst case, this includes additional runtime processes). A substantial amount of KDE-provided functionality should be provided by Qt, given that it would benefit all Qt applications, not just KDE applications. These pieces have been moved to Qt 5, thanks to the new Open Governance of the Qt project.

On top of this enriched Qt 5, KDE Frameworks provides additional functionality. Making things modular has required some important API changes in the core classes used by all KDE applications. These changes will be presented and discussed. Last chance to complain!

Finally, the benefits for non-KDE Qt applications will be presented. The distinction between a Qt application and a KDE application is being completely reevaluated. The goal from a technical (API) point of view is for that distinction to disappear completely.
<p>&nbsp; </p>
<h2>David Faure</h2>
David Faure, very-long-time KDE developer (some say "dinosaur"), sponsored by Nokia to work on KDE, and a director of KDAB France. Maintainer of many parts of the KDE frameworks (component technology, network transparency, reorganization). Contributes to Qt 5 as needed by the KDE Frameworks.
