---
title:       "Building new KDE ecosystems: KDE Connect"
date:        2012-05-10
changed:     2012-05-13
aliases:     [ /2012/node/21 ]

---
<strong>Speaker:</strong> Agustín Benito Bethencourt

Support from different types of entities (especially financial support) is critical to key activities that make KDE successful. The KDE Connect Program is a way for KDE to build strong and lasting relationships with other organizations. KDE Connect is designed to leverage our strengths and to accomplish several goals, especially economic sustainability and independence. The KDE Connect Program is a project launched by KDE to engage organizations worldwide. 


The first part of the talk will explain the fundamentals of KDE Connect that led to the current design, and how we got to the point where we are at Akademy 2012. 

How is KDE engaging these organizations? What kind of activities and services are we offering? What kind of organizations are getting involved? Why? These questions will be addressed during the second part of the talk, while describing actions taken so far by KDE Connect Team.

The third part will deal with the conclusions reached, lessons learned, and the steps that KDE will or should take in order to improve the results. Open questions will also be considered.
<p>&nbsp; </p>
<h2>Agustín Benito Bethencourt</h2>
Agustín has experience as an entrepreneur, FLOSS business association promoter in Spain, IT program/project manager and business developer. He has a bachelor's degree in Applied Physics. He has been a KDE contributor since 2005, KDE Spain co-founder, KDE e.V. member and the most recent addition to the KDE e.V. Board of Directors.
