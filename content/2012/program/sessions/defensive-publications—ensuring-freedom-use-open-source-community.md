---
title:       "Defensive Publications—Ensuring Freedom Of Use In The Open Source Community"
date:        2012-05-10
changed:     2012-06-28
aliases:     [ /2012/node/24 ]

---
<strong>Speaker:</strong> Raffi Gostanian


Defensive publications are documents that provide descriptions and artwork of a product, device or method, so that it enters the public domain and becomes prior art. This powerful preemptive disclosure prevents other parties from obtaining a patent on the product, device or method. It enables the original developers or inventors to ensure that they have access to their own work by preventing others from later making patent claims on it. It also means that they do not have to bear the cost of patent applications. Defensive Publications are endorsed by the United States Patent and Trademark Office (USPTO) as an IP rights management tool.

The Defensive Publications Program is a part of Linux Defenders, which is supported by the Open Invention Network (OIN). It enables developers and other non-attorneys to submit defensive publications quickly by using a simple Web-based form. After submission the defensive publication is reviewed and edited as needed by OIN personnel at no charge. The completed defensive publication is then added to the IP.com database, which is used by Examiners at the USPTO to search for prior art when examining patent applications.

To prevent future patent wars and unencumbered use and reuse of Open Source code, we need your help to create defensive publications. At this conference, OIN will have several sessions where you can bring your ideas for inclusion into the Defensive Publication program. We will sit down with you, explain how to write a defensive publication for your idea and help you write and submit it. If you have a completed idea described in a document, please bring it to one of the sessions.  If you are interested, please review these <a href="http://www.defensivepublications.org/defensive-pubs-examples" data-proofer-ignore>examples of Defensive Publications</a>. 
<p>&nbsp; </p>
<h2>Raffi Gostanian</h2>
Raffi Gostanian is Chief Operating Officer at Open Invention Network, an entity that enables innovation in open source and a vibrant ecosystem around Linux.  

Raffi has held a variety of software development and senior management positions with private corporations, such as GlobeRanger Corporation, pre- and post-IPO corporations, such as Inet Technologies Inc., and public multinationals such as Nortel Networks.  Raffi has also been the Chief Strategy Officer for a private software company in Dallas. During his tenure at these technology companies, Raffi developed and taught courses for the computer science program at the University of Texas at Dallas, for MCI and Bell Northern Research (the R&D subsidiary of Nortel Networks), as well as  the United States Telecommunications Training Institute (a joint venture between the Federal Government and telecom companies that provides training to employees who manage communications infrastructure in the developing countries of the world).

Raffi received a M.B.A. from the University of Texas at Dallas, a M.S. in Telecommunications Systems from Southern Methodist University, and a B.S. in Electrical Engineering from Texas A&M University.
