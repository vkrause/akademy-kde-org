---
title:       "Hotel & Travel"
date:        2012-01-30
changed:     2012-03-05
menu:
  "2012":
    parent: travel
    weight: 1
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/media/2012/TallinnPanoramaA2012Website_0.jpg"><img src="/media/2012/TallinnPanoramaA2012Website350_0.jpg" /></a><br /><small><small>courtesy of Tallinn City Tourist Office & Convention Bureau</small></small></div>


Tallinn is located on the southern coast of the Gulf of Finland. It is home to about 400,000 people. Tallinn is famous for its beautiful medieval old town which is listed as one of the UNESCO World Heritage Sites. The city was the European Capital of Culture in 2011.

Estonia's official language is Estonian. Russian, Finnish, English and German are also understood and widely spoken.

There are public Internet access points all over Estonia, located in local libraries and post offices. There are also over 100 free wireless Internet zones around the country.

With its vibrant technology scene, Tallinn is home to the Tehnopol Science and Business Park with 150 companies, a start-up incubator and two institutions of higher learning—the Tallinn University of Technology and the Estonian Information Technology College. The latter will play host to Akademy 2012. For more details, see <a href="../venue">Venue</a>.

<strong>More information about recommended hotels and hostels can be found <a href="../accommodation">here</a>. Information about the nearest airports and public transport can be found <a href="../travel">here</a>.</strong>

Estonia is part of the Schengen Visa area. If you need to apply for a visa to travel to Estonia, please contact the Akademy Team at akademy-team at kde dot org for invitation letters and other visa requirements. Please provide information on your previous involvement with KDE or why you wish to attend the event.

KDE e.V. may reimburse travel costs for those visiting KDE conferences and events. If you find money to be a problem in participating in Akademy, read more about the reimbursement policy on the <a href="http://ev.kde.org/rules/reimbursement_policy.php">KDE e.V. website</a>.
