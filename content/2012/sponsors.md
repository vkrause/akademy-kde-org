---
title:       "Sponsors"
date:        2012-05-08
changed:     2013-03-06
menu:
  "2012":
    parent: sponsors
    weight: 1
---
<p>A big thank you to our sponsors who help to make this event happen! There are still sponsorship opportunities. If you are interested, please visit <a href="/2012/sponsoring">Sponsoring Akademy 2012</a> for more information, including valuable sponsor benefits.</p>
<p>&nbsp; </p>

<h2><big>Platinum Sponsors</big></h2>

<div style="float: right; padding: 1ex; margin: 1ex;"><a href="http://blue-systems.com/"><img src="/media/2012/BlueNew300.png" /></a></div>
<p id="bluesystems"><big><strong>Blue Systems</strong></big> is investing in Free and Open Source technologies.</p>
<p>They sponsor the development of KDE projects and distributions like Kubuntu, Netrunner and Linux MintKDE. Their goal is "To offer solutions for people valuing freedom and choice".</p>

<div style="float: right; padding: 1ex; margin: 1ex;"><a href="http://www.nokia.com/"><img src="/media/2012/Nokia_logo_20p.png" /></a></div>
<p id="nokia"><big><strong>Nokia</strong></big> is a global leader in mobile communications whose products have become an integral part of the lives of people around the world. Every day, more than 1.3 billion people use a Nokia to capture and share experiences, access information, find their way or simply to speak to one another. Nokia's technological and design innovations have made its brand one of the most recognized in the world. Nokia is also the main contributor to the Qt Project. For more information, visit <a href="http://www.nokia.com/about-nokia">About Nokia</a> and the <a href="http://www.developer.nokia.com/Develop/Qt/" data-proofer-ignore>Qt Developer</a>pages.</p>
<p>&nbsp; </p>
<h2><big>Silver Sponsors</big></h2>
<div style="float: right; padding: 1ex; margin: 1ex;"><a href="http://qt.digia.com"><img src="/media/2012/digiaLogoPage.png" /></a></div>
<p id="digia"><big><strong>Digia</strong></big> (NASDAQ OMX Helsinki DIG1V) is the worldwide exclusive commercial licensor of Qt. Digia has over 10 years of experience with worldwide Qt application, UI development, testing, UX design services and consulting. Digia is a recognized global leader in the Qt ecosystem and committed to the future of Qt for desktop and embedded development. Digia's pioneering Qt Commercial product, support and services offering provides developers and companies a one-stop shop for all Qt development needs.</p>
<br />

<div style="float: right; padding: 1ex; margin: 1ex;"><a href="http://code.google.com/opensource/"><img src="/media/2012/GoogleLogoPage.png" /></a></div>
<p id="google"><big><strong>Google</strong></big> is a proud user and supporter of open source software and development methodologies. Google contributes back to the Open Source community in many ways, including more than 20 million lines of source code, project hosting on Google Code, projects for students including Google Summer of Code and the Google Code-in Contest, and support for a wide variety of projects, LUGS, and events around the world.</p>
<br />

<div style="float: right; padding: 1ex; margin: 0ex 6ex 1ex 1ex;"><a href="https://01.org/"><img src="/media/2012/IntelPage.png" width="190" /></a></div>
<p id="intel"><big><strong>Intel’s Open Source Technology Center</strong></big> is the heart of open source development at Intel.  Our leadership in upstream contributions, maintainership and innovation shapes the performance and user experience of a wide variety of open source projects. 
<p>&nbsp; </p>
<p>&nbsp; </p>
<h2><big>Bronze Sponsors</big></h2>

<div style="float: right; padding: 1ex; margin: 1ex;"><a href="http://www.collabora.com/"><img src="/media/2012/collabora-logo300.png" /></a></div>
<p><big><strong>Collabora</strong></big> is a multinational Open Source consultancy specializing in bringing companies and the Open Source software community together. They provide a full range of services based around Open Source technologies including architecture, software development, project management, infrastructure and community development. Collabora are skilled in a wide range of areas from consumer devices to multimedia and real-time communications systems.</p>

<div style="float: right; padding: 1ex; margin: 1ex;"><a href="http://www.zabbix.com/"><img src="/media/2012/zabbixPage.png" /></a></div>
<p><big><strong>Zabbix SIA</strong></big> is the developer of a popular enterprise-class open source distributed monitoring solution "Zabbix". The company provides a wide range of services, including technical and consultative support, integration, implementation and customized development services as well as Zabbix professional training. The monitoring solution is used across all industries including Finance and Insurance, IT&T, Health Care, Public Sector, Retail, Energy and Chemicals</p>
<p>&nbsp; </p>
<h2><big>Supporters</big></h2>

<div style="float: right; padding: 1ex; margin: 1ex;"><a href="http://www.froglogic.com/"><img src="/media/2012/froglogicPage.png" /></a></div>
<p><big><strong>froglogic GmbH</strong></big> is a software company based in Hamburg, Germany. Their flagship product is Squish, the market-leading automated cross-platform testing tool for GUI applications based on Qt, KDE, Java AWT/Swing and SWT/RCP, Windows MFC and .NET WindowsForms, Mac OS X Carbon/Cocoa, iOS Cocoa Touch and for HTML/Ajax-based Web applications running in various Web browsers.</p>

<div style="float: right; padding: 1ex; margin: 1ex;"><a href="http://www.redflag-linux.com/en/" data-proofer-ignore><img src="/media/2012/redflag-logoPage.jpg" /></a></div>
<p>Since its founding in June 2000, <big><strong>Red Flag Software</strong></big> has grown rapidly to become the largest Linux company in Asia with more than 200 employees. Their headquarters is located in Beijing, and subsidiaries in Guangzhou and Shanghai. The product line includes high-end Linux server OS, cluster system, desktop OS, embedded system, technical support services and training.</p>
<p>&nbsp; </p>
<h2><big>Media Partners</big></h2>

<div style="float: right; padding: 1ex; margin: 1ex;"><a href="http://www.linux-magazine.com/"><img src="/media/2012/LM_LogoPage.jpg" /></a></div>
<big><strong>Linux Magazine</strong></big> is a monthly magazine serving readers in over 50 countries.

Linux Magazine is published in <a href="http://www.linux-magazine.com/">English</a>,<a href="http://www.linux-magazin.de/"> German</a>, <a href="http://www.linux-magazine.es/">Spanish</a> and <a href="http://www.linux-magazine.pl/">Polish</a>.
